<?php if(isset($message)) { ?>
<div class="alert alert-success" role="alert"><?php echo $message; ?></div>
<?php } ?>

<table class="table">
    <thead>
        <tr>
            <th scope="col">Identifiant</th>
            <th scope="col">Nom</th>
            <th scope="col">Prénom</th>
            <th scope="col">Mot de passe</th>
        </tr>
    </thead>
    <tbody>
        <?php foreach ($users as $user): ?>
            <?php $banurl = base_url().'index.php/Comics/ban_user/'.$user['login']; ?>
            <?php $unbanurl = base_url().'index.php/Comics/unban_user/'.$user['login']; ?>
            <tr>
                <td><?php echo $user['login'];?></td>
                <td><?php echo $user['name'];?></td>
                <td><?php echo $user['firstname'];?></td>
                <td><?php echo $user['password'];?></td>
                <?php if($user['admin']!='t') { ?>
                    <?php if($user['banned']=='t') { ?>
                        <td><button onclick="location.href='<?php echo $unbanurl; ?>';" style="float:right " class="btn btn-success" type="submit">DEBANNIR</button></td>
                    <?php } else { ?>
                        <td><button onclick="location.href='<?php echo $banurl; ?>';" style="float:right " class="btn btn-danger" type="submit">BANNIR</button></td>
                    <?php } ?>
                 <?php } ?>
            </tr>
        <?php endforeach ?>
    </tbody>
</table>