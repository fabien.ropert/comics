<!DOCTYPE html>
<html>

<head>
    <title>Comics</title>
    <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>css/style.css">
    <script src="<?php echo base_url();?>jquery/jquery-3.4.1.min.js"></script>
    <script src="<?php echo base_url();?>bootstrap/js/bootstrap.min.js"></script>
    <link rel="stylesheet" href="<?php echo base_url();?>fontawesome/css/all.css">
</head>
<nav class="navbar navbar-expand-lg navbar-dark bg-dark">
    <a class="navbar-brand" href="<?php echo base_url();?>"><span class="badge badge-primary">Projet PHP</span></a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarText" aria-controls="navbarText" aria-expanded="false" aria-label="Toggle navigation">
    	<span class="navbar-toggler-icon"></span>
  	</button>
  	<div class="collapse navbar-collapse" id="navbarText">
	    <ul class="navbar-nav mr-auto">
	    	<?php if($contenu=='home') { ?>
	    	    <li class="nav-item active">
	    	<?php } else { ?>
	            <li class="nav-item">
	    	<?php } ?>
	            <a class="nav-link" href="<?php echo base_url();?>"><i class="fas fa-book-open"></i> Comics</a>
	        </li>

	        <?php if($this->session->userdata('login')) { ?>
	           <?php if($contenu=='my_comics') { ?>
	    	        <li class="nav-item active">
	    	    <?php } else { ?>
	                <li class="nav-item">
	    	    <?php } ?>
	                <a class="nav-link" href="<?php echo base_url();?>index.php/Comics/my_comics/"><i class="fas fa-folder-open"></i> Ma collection</a>
	       </li>

	        <?php if($this->session->userdata('admin')=='t') { ?>
	            <?php if($contenu=='admin') { ?>
	                <li class="nav-item active">
	            <?php } else { ?>
	                <li class="nav-item">
	            <?php } ?>
	                <a style="color:red!important;" class="nav-link" href="<?php echo base_url();?>index.php/Comics/admin/"><i class="fas fa-gavel"></i> Administration</a>
	        </li>
	        <?php } } ?>
	    </ul>
	    <ul class="navbar-nav ml-auto">
			<?php if($this->session->userdata('login')) { ?>
                <li class="nav-item dropdown ml-auto">
                    <a class="nav-link dropdown-toggle" href="#" id="navbardrop" data-toggle="dropdown"><b><?php echo $this->session->userdata('firstname'); ?> <?php echo $this->session->userdata('name'); ?></b></a>
                    <div class="dropdown-menu">
                        <a class="dropdown-item" href="<?php echo base_url();?>index.php/Comics/logout/">Déconnexion</a>
                    </div>
                </li>
			<?php } else { ?>
			    <?php if($contenu=='login') { ?>
	    	        <li class="nav-item active">
	    	    <?php } else { ?>
	                <li class="nav-item">
	    	<?php } ?>
	            <a class="nav-link" href="<?php echo base_url();?>index.php/Comics/login/"><i class="fas fa-sign-in-alt"></i> Se connecter</a>
	        </li>
	        <?php if($contenu=='register') { ?>
	    	    <li class="nav-item active">
	    	<?php } else { ?>
	            <li class="nav-item">
	    	<?php } ?>
	            <a class="nav-link" href="<?php echo base_url();?>index.php/Comics/register/"><i class="fas fa-user-plus"></i> S'inscrire</a>
	        </li>
	        <?php } ?>
	    </ul>
	</div>
</nav>
<body>
	<div class="container-fluid">
    	<h2><?php echo $titre_page; ?></h2>
		<?php $this->load->view($contenu); ?>
    </div>
    <footer>
        <span>Projet Réalisé par :
            <b>Fabien ROPERT</b> -
            <b>Charlotte LE COZ</b> -
            <b>Maryne GUEVEL</b>
        </span>
    </footer>
</body>

</html>
