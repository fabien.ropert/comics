<?php if(isset($error)) { ?>
<div class="alert alert-danger" role="alert"><?php echo $error; ?></div>
<?php } ?>

<?php if(isset($message)) { ?>
<div class="alert alert-success" role="alert"><?php echo $message; ?></div>
<?php } ?>

<table class="table">
    <thead>
        <tr>
			<th scope="col">#</th>
      		<th scope="col">Série</th>
      		<th scope="col">Numéro</th>
      		<th scope="col">Date</th>
      		<th scope="col">Couverture</th>
      		<?php if($this->session->userdata('login')) { ?>
                <th scope="col"></th>
            <?php } ?>
    	</tr>
    </thead>
    <tbody>
        <?php foreach ($comics as $comic): ?>
            <?php $url = base_url().'index.php/Comics/add_comic/'.$comic['comic_id']; ?>
            <tr>
                <td><?php echo $comic['comic_id'];?></td>
                <td><?php echo $comic['serie'];?></td>
                <td><?php echo $comic['numero'];?></td>
                <td><?php echo $comic['date'];?></td>
                <td><img style="max-width:25px;" src="<?php echo $comic['couverture'];?>"></td>
                <?php if($this->session->userdata('login')) { ?>
                    <?php if($this->ComicsModel->checkIfComicAlreadyInList($this->session->userdata('login'),$comic['comic_id'])==0) { ?>
                         <td><button onclick="location.href='<?php echo $url; ?>';" style="float:right "class="btn btn-primary" type="submit">AJOUTER A MA COLLECTION</button></td>
                    <?php } else { ?>
                        <td><button onclick="location.href='<?php echo $url; ?>';" style="float:right "class="btn btn-primary" disabled type="submit">AJOUTER A MA COLLECTION</button></td>
                    <?php } ?>
                <?php } ?>
            </tr>
        <?php endforeach ?>
	</tbody>
</table>