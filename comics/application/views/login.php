<?php echo form_open('Comics/login_process'); ?>
<?php if(isset($error)) { ?>
    <div class="alert alert-danger" role="alert"><?php echo $error; ?></div>
<?php } ?>

<form>
	<div class="from-group" style="width:50%;margin:0 auto">
		<label for="login">Nom d'utilisateur</label>
		<input type="text" class="form-control" placeholder="Nom d'utilisateur" id="login" name="login"><br>
		<label for="login">Mot de passe</label>
		<input type="password" class="form-control" placeholder="Mot de passe" id="pass" name="pass"></label><br>
		<div class="text-right">
			<button class="btn btn-primary" type="submit">Se connecter</button>
		</div>
	</div>
</form>
