<?php echo form_open('Comics/register_process'); ?>
<?php if(isset($error)) { ?>
    <div class="alert alert-danger" role="alert"><?php echo $error; ?></div>
<?php } ?>

<form>
	<div class="form-row"style="width:50%;margin:0 auto">
		<div class="form-group col-md-6">
			<label for="lastname">Nom</label>
			<input type="text" class="form-control" placeholder="Nom" id="lastname" name="lastname"></label><br>
		</div>
    	<div class="form-group col-md-6">
			<label for="firstname">Prenom</label>
	   		<input type="text" class="form-control" placeholder="Prénom" id="firstname" name="firstname"></label><br>
		</div>
	</div>
	<div class="form-group" style="width:50%;margin:0 auto">
        <label for="login">Nom d'utilisateur</label>
        <input type="text" class="form-control" placeholder="Nom d'utilisateur" id="login" name="login"></label><br>
        <label for="pass">Mot de passe</label>
        <input type="password" class="form-control" placeholder="Mot de passe" id="pass" name="pass"></label><br>
        <label for="confpass">Confirmation</label>
        <input type="password" class="form-control" placeholder="Confirmation" id="confpass" name="confpass"></label><br>
        <div class="text-right">
            <button class="btn btn-primary" type="submit">S'inscrire</button>
        </div>		
    </div>
</form>