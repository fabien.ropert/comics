<?php
class ComicsModel extends CI_Model {
    
	public function __construct() {
		$this->load->database();
	}
	
	public function getComics() {
		$this->db->order_by('date', 'DESC');
		$query = $this->db->get('comics._comic');
		return $query->result_array();
	}

	public function getUsers() {
		$this->db->order_by('login');
		$query = $this->db->get('comics._collector');
		return $query->result_array();
	}

	public function banUser($login) {
		$this->db->set('banned', 'true', FALSE);
		$this->db->where('login', $login);
		$this->db->update('comics._collector');
		$data = array('collector_login'=>$this->session->userdata('login'));
		return $this->db->delete('comics._collect', $data);
	}

	public function unbanUser($login) {
		$this->db->set('banned', 'false', FALSE);
		$this->db->where('login', $login);
		$this->db->update('comics._collector');
	}

    public function addCollector($login, $name, $firstname, $password) {
		$data=array('login'=>$login, 'name'=>$name, 'firstname'=>$firstname, 'password'=>$password) ;
		return $this->db->insert('comics._collector', $data) ;
    }

    public function checkLogin($login, $pass) {
  		$this->db->where('login', $login);
		$this->db->where('password', $pass);
		$this->db->from("comics._collector");
		return $this->db->count_all_results();
	 }
	 
	public function checkRegister($login) {
		$this->db->where('login', $login);
	  	$this->db->from("comics._collector");
	  	return $this->db->count_all_results();
    }

 	public function getUserData($login) {
 		$this->db->where('login', $login);
		$query = $this->db->get('comics._collector');
		return $query->result_array();
 	}

 	public function getUserComics($login) {
 		$this->db->from('comics._collect');
 		$this->db->join('comics._comic','comics._collect.comic_id=comics._comic.comic_id');
 		$this->db->where('collector_login', $login);
		$this->db->order_by('add_date', 'DESC');
		$query = $this->db->get();
		return $query->result_array();
	}

	public function getComicsCount($login) {
		$this->db->where('collector_login', $login);
	  	$this->db->from("comics._collect");
	  	return $this->db->count_all_results();
	}

	public function checkIfComicAlreadyInList($login,$comic_id) {
		$this->db->where('collector_login', $login);
		$this->db->where('comic_id', $comic_id);
	  	$this->db->from("comics._collect");
	  	return $this->db->count_all_results();
	}
	 
	public function addComicToList($comic_id){
		$timestamp = date('Y-m-d H:i:s');
		$data = array('comic_id'=>$comic_id, 'collector_login'=>$this->session->userdata('login'), 'add_date'=>$timestamp);
		return $this->db->insert('comics._collect', $data);
	}

	public function removeComicFromList($comic_id){
		$data = array('comic_id'=>$comic_id, 'collector_login'=>$this->session->userdata('login'));
		return $this->db->delete('comics._collect', $data);
	}
}
?>
