<?php
class Comics extends CI_Controller {
    
	public function __construct() {
		parent::__construct();
		$this->load->helper('url');
		$this->load->helper('form');
		$this->load->library('form_validation');
		$this->load->library('session');
		$this->load->model('ComicsModel');  
	}
    
	public function index() {
		$data['comics'] = $this->ComicsModel->getComics();
		$data['titre_page'] = "Comics";
		$data['contenu'] = "home";
		$this->load->vars($data);
		$this ->load ->view('template');
	}
    
	public function login() {
		$data['titre_page'] = "Connexion";
		$data['contenu'] = "login";
		$this->load->vars($data);
		$this ->load ->view('template');
	}
    
	public function register() {
		$data['titre_page'] = "Inscription";
		$data['contenu'] = "register";
		$this->load->vars($data);
		$this ->load ->view('template');
	}

	public function my_comics() {
		if($this->session->userdata('login')) {
			$data['comics_count'] = $this->ComicsModel->getComicsCount($this->session->userdata('login'));
		    $data['comics'] = $this->ComicsModel->getUserComics($this->session->userdata('login'));
		    $data['titre_page'] = "Ma collection";
			$data['contenu'] = "my_comics";
			$this->load->vars($data);
			$this ->load ->view('template');
		} else {
			redirect(base_url().'index.php/Comics/login/');
		}
	}

	public function admin() {
		if($this->session->userdata('login')) {
			if($this->session->userdata('admin')=='t') {
			    $data['users'] = $this->ComicsModel->getUsers();
			    $data['titre_page'] = "Administration";
				$data['contenu'] = "admin";
				$this->load->vars($data);
				$this ->load ->view('template');
			} else {
				redirect(base_url().'index.php');
			}
		} else {
			redirect(base_url().'index.php/Comics/login/');
		}
	}

	public function add_comic($comic_id) {
		if($this->session->userdata('login')) {
			if($this->ComicsModel->checkIfComicAlreadyInList($this->session->userdata('login'),$comic_id)==0) {
				if($this->ComicsModel->getComicsCount($this->session->userdata('login'))<10) {
					$this->ComicsModel->addComicToList($comic_id);
					$data['message'] = "Le comic a été ajouté à votre collection";
					$data['titre_page'] = "Comics";
					$data['contenu'] = "home";
					$data['comics'] = $this->ComicsModel->getComics();
					$this->load->vars($data);
					$this ->load ->view('template');
				} else {
					$comics_list=$this->ComicsModel->getUserComics($this->session->userdata('login'));
					$this->ComicsModel->removeComicFromList($comics_list[0]['comic_id']);
					$this->ComicsModel->addComicToList($comic_id);
					$data['message'] = "Le comic a été ajouté à votre collection en remplacant le plus récent";
					$data['titre_page'] = "Comics";
					$data['contenu'] = "home";
					$data['comics'] = $this->ComicsModel->getComics();
					$this->load->vars($data);
					$this ->load ->view('template');
				}
			} else {
				$data['error'] = "Ce comic est déjà dans votre collection";
			    $data['titre_page'] = "Comics";
			    $data['contenu'] = "home";
			    $data['comics'] = $this->ComicsModel->getComics();
			    $this->load->vars($data);
			    $this ->load ->view('template');
			}
		} else {
			redirect(base_url().'index.php/Comics/login/');
		}
	}

	public function remove_comic($comic_id) {
		if($this->session->userdata('login')) {
			$this->ComicsModel->removeComicFromList($comic_id);
			$data['message'] = "Le comic a été supprimé de votre collection";
			$data['comics_count'] = $this->ComicsModel->getComicsCount($this->session->userdata('login'));
		    $data['comics'] = $this->ComicsModel->getUserComics($this->session->userdata('login'));
		    $data['titre_page'] = "Ma collection";
			$data['contenu'] = "my_comics";
			$this->load->vars($data);
			$this ->load ->view('template');
		} else {
			redirect(base_url().'index.php/Comics/login/');
		}
	}

	public function ban_user($login) {
		if($this->session->userdata('login')) {
			if($this->session->userdata('admin')=='t') {
				$this->ComicsModel->banUser($login);
				$data['message'] = "L'utilisateur a été bannis";
			    $data['users'] = $this->ComicsModel->getUsers();
			    $data['titre_page'] = "Administration";
				$data['contenu'] = "admin";
				$this->load->vars($data);
				$this ->load ->view('template');
			} else {
				redirect(base_url().'index.php');
			}
		} else {
			redirect(base_url().'index.php/Comics/login/');
		}
	}

	public function unban_user($login) {
		if($this->session->userdata('login')) {
			if($this->session->userdata('admin')=='t') {
				$this->ComicsModel->unbanUser($login);
				$data['message'] = "L'utilisateur a été débannis";
			    $data['users'] = $this->ComicsModel->getUsers();
			    $data['titre_page'] = "Administration";
				$data['contenu'] = "admin";
				$this->load->vars($data);
				$this ->load ->view('template');
			} else {
				redirect(base_url().'index.php');
			}
		} else {
			redirect(base_url().'index.php/Comics/login/');
		}
	}
    
	public function login_process() {
		$this->form_validation->set_rules('login', 'Identifiant', 'required');
		$this->form_validation->set_rules('pass', 'Mot de passe', 'required');
		if($this->form_validation->run())
		{
		    $result = $this->ComicsModel->checkLogin($this->input->post('login'), sha1($this->input->post('pass')));
		    if($result == '1') {
		        $userdata = ($this->ComicsModel->getUserData($this->input->post('login')))[0];
		        if($userdata['banned']=='f') {
			        $this->session->set_userdata($userdata);
			        redirect(base_url().'index.php/Comics/my_comics/');
			    } else {
				    $data['error'] = "Vous êtes bannis";
			        $data['titre_page'] = "Connexion";
			        $data['contenu'] = "login";
			        $this->load->vars($data);
			        $this ->load ->view('template');
			    }
		    } else {
		        $data['error'] = "Identifiants incorrects";
		        $data['titre_page'] = "Connexion";
		        $data['contenu'] = "login";
		        $this->load->vars($data);
		        $this ->load ->view('template');
		    }
		} else {
		    $data['titre_page'] = "Connexion";
		    $data['contenu'] = "login";
		    $this->load->vars($data);
		    $this ->load ->view('template');
		}
	}
    
	public function register_process() {
		$this->form_validation->set_rules('lastname', 'Nom', 'required');
		$this->form_validation->set_rules('firstname', 'Prenom', 'required');
		$this->form_validation->set_rules('login', 'Identifiant', 'required');
		$this->form_validation->set_rules('pass', 'Mot de passe', 'required');
		$this->form_validation->set_rules('confpass', 'Confirmation', 'required');
		if($this->form_validation->run())
		{
			if($this->input->post('pass')==$this->input->post('confpass')) {
				$result = $this->ComicsModel->checkRegister($this->input->post('login'));
				if($result == '0') {
				    $this->ComicsModel->addCollector($this->input->post('login'),$this->input->post('lastname'),$this->input->post('firstname'),sha1($this->input->post('pass')));
				    redirect(base_url().'index.php/Comics/my_comics/');
				} else {
				    $data['error'] = "Utilisateur déjà enregistré";
				    $data['titre_page'] = "Inscription";
				    $data['contenu'] = "register";
				    $this->load->vars($data);
				    $this ->load ->view('template');
				}
			} else {
				$data['error'] = "Les mots de passe ne correspondent pas";
				$data['titre_page'] = "Inscription";
				$data['contenu'] = "register";
				$this->load->vars($data);
				$this ->load ->view('template');
			}
		} else {
			$data['titre_page'] = "Inscription";
			$data['contenu'] = "register";
			$this->load->vars($data);
			$this ->load ->view('template');
		}
	}

	public function logout() {
		$this->session->sess_destroy();
		redirect(base_url().'index.php');
	}
}
?>

